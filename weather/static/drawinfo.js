'use strict';

/* With JQuery

var loadWeather = function loadWeather () {
	$.get('http://api.openweathermap.org/data/2.5/find?q=Lyon,fr&units=metric', function(response) {
			var areaText = $('#areaText');

		areaText.empty();

		areaText.append('<h2>' + response.list[0].name + '</h2>');
		areaText.append('<p>' + response.list[0].main.temp + '</p>');
	})
		.fail(function() {
			alert('Couldn\'t get blog posts from server.');
	});


}



$(document).ready(function() {
	loadWeather();
});

var loadWeather = function loadWeather () {
	var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() définie dans la partie précédente

	xhr.open("GET", "http://api.openweathermap.org/data/2.5/find?q=Lyon,fr&units=metric", true);
	//xhr.send(null);


}*/



function loadWeather()
{
            var ajaxObject ;
            var cityName = document.getElementById("city").value;
            var unit = document.getElementById("unit").value;
     
            // code for IE7+, Firefox, Chrome, Opera, Safari
            if (window.XMLHttpRequest) {
                ajaxObject = new XMLHttpRequest();
                     //  http://api.openweathermap.org/data/2.5/find?q=Lyon,fr&units=Celsius
                var url = "http://api.openweathermap.org/data/2.5/find?q=" + cityName + ",fr&units=" + unit;
                  
                ajaxObject.open("GET",url,true);
                ajaxObject.send();
                  
                ajaxObject.onreadystatechange = function() {
                    if (ajaxObject.readyState == 4 && ajaxObject.status == 200 ) {

                        document.getElementById("cityName").innerHTML = "";
                        document.getElementById("temp").innerHTML = "";
                        document.getElementById("weather").innerHTML = "";
                    
                        var obj = JSON.parse(ajaxObject.responseText);

						if ( obj.count != 0)
						{
	                        document.getElementById("cityName").innerHTML =  obj.list[0].name ;



	                        document.getElementById("temp").innerHTML =  
	                        "Temperature " + obj.list[0].main.temp + " " + unit + " (" + obj.list[0].main.temp_min + "-"+ obj.list[0].main.temp_max + ")<br>" +
	                        "Pressure " + obj.list[0].main.pressure + "<br>" +
	                        "Humidity " + obj.list[0].main.humidity + "<br>" ;

	                        document.getElementById("weather").innerHTML = "Wind speed " + obj.list[0].wind.speed;
	                    }
	                    else
	                    {
	                    		document.getElementById("cityName").innerHTML =  cityName ;
	                    		alert(cityName +  " does not exist");

	                    }



                    }
                }
            } 
}

/*
 Feed the dataList with proposed cities
*/
function feedDataList()
{
 	var ajaxObject ;
            var cityName = document.getElementById("city").value;
            var unit = document.getElementById("unit").value;
          
              
            // code for IE7+, Firefox, Chrome, Opera, Safari
            if (window.XMLHttpRequest) {
                ajaxObject = new XMLHttpRequest();
                var url = "http://api.openweathermap.org/data/2.5/find?q=" + cityName + ",fr&units=" + unit;


                ajaxObject.open("GET",url,true);
                ajaxObject.send();
                  
                ajaxObject.onreadystatechange = function() {
                    if (ajaxObject.readyState == 4 && ajaxObject.status == 200 ) {

						var obj = JSON.parse(ajaxObject.responseText);

						var dataList=document.getElementById("cities"); 
						while (dataList.firstChild) {
						    dataList.removeChild(dataList.firstChild);
						}
                    	
                    
                    	for (var i = 0; i<obj.count;i++)
                    	{
                    		var opt = document.createElement("option");
			                opt.value = obj.list[i].name;
			           
			                dataList.appendChild(opt);

                    	}
                    }
                }
            } 

}

document.getElementById("myForm").onkeyup = function(e) { 

	// Enter is pressed
	if (e.keyCode == 13)
		loadWeather();
	else
		return feedDataList(); 
}



loadWeather();
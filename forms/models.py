from application import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    email_address = db.Column(db.String, nullable=False)
    birthday = db.Column(db.DateTime)
    registered_at = db.Column(db.DateTime, nullable=False)
    password = db.Column(db.String, nullable=False)
    @staticmethod
    def get(email_address, password):
        rv = User.query.filter_by(email_address=email_address).filter_by(password=password).first()
        return rv
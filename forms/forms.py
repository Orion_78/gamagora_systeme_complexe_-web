from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email, Optional, Required, EqualTo
from wtforms.fields.html5 import EmailField, DateField

class RegistrationForm(Form):
    name = StringField('Your full name', validators=[DataRequired()])
    email_address = EmailField('Your email address', validators=[DataRequired()])
    birthday = DateField('Your birthday', validators=[Optional()]) # format='%d-%m-%Y',
    password = PasswordField('New Password', validators=[DataRequired(), EqualTo('passwordConfirm', message='Passwords must match')])
    passwordConfirm  = PasswordField('Repeat Password')


class LoginForm(Form):
	email_address = EmailField('Your email address', validators=[DataRequired()])
	password = PasswordField('Your password', validators=[DataRequired()])
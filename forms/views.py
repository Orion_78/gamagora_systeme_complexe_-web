import datetime
from flask import render_template, redirect, request, flash, url_for, session, g 
from application import app, db
from models import User
from forms import RegistrationForm, LoginForm


@app.template_filter('datetimeformat')
def datetimeformat(value, format='%H:%M / %d-%m-%Y'):
    return value.strftime(format)


@app.route('/')
def index():
    users = User.query.all()
    return render_template('index.html', users=users)


@app.route('/api/register', methods=('GET', 'POST'))
def register():
    oldPath = request.args.get('oldPath', None)
    if g.formregister.validate_on_submit():
        add_user_from_form(g.formregister.name.data, g.formregister.email_address.data, g.formregister.password.data, g.formregister.birthday.data)
        return redirect(oldPath)
    else:
        return render_template('loginregister_wrong.html', oldPath=oldPath)    
    


@app.route('/api/login', methods=('GET', 'POST'))
def login():
    oldPath = request.args.get('oldPath', None)

    if g.formlogin.validate_on_submit():
        g.user = User.get(email_address=g.formlogin.email_address.data,password=g.formlogin.password.data)
        if g.user is not None:
            session['user_id'] = g.user.id
            flash('User ' + g.user.name + ' has been logged!')
            return redirect(oldPath)
        else:
            g.formlogin.email_address.errors.append('doesn\'t exist')
    
    return render_template('loginregister_wrong.html', oldPath=oldPath)
   

@app.route('/api/logout')
def logout():
    if g.user is not None:
        flash('User ' + g.user.name + ' has been logout!')
    else:
        flash('You are already logged out!')
    g.user = None
    session.pop('user_id', None)

    return redirect(request.referrer)


def add_user_from_form(_name, _email_address, _password, _birthday=None):
    new_user = User()
    new_user.name = _name
    new_user.email_address = _email_address
    new_user.password = _password
    if _birthday:
        new_user.birthday = _birthday
    new_user.registered_at = datetime.datetime.today()
    db.session.add(new_user)
    db.session.commit()
    flash('User ' + _name + ' has been registered!')



@app.before_request
def before_request():
    if 'user_id' in session:
        g.user = User.query.get(session['user_id']) # session['user_id']
    else:
        g.user = None
        g.formregister = RegistrationForm()
        g.formlogin = LoginForm()

